    <div class="container">
        <br>
        <div class="row justify-content-center">
            <div class="col-md-8">
                <div class="card">
                    <div class="card-header">Register</div>
                        <?php echo validation_errors('<br><div style="color:red; text-align: center;">', '</div><br>'); ?>
                        <div style="color:red; text-align: center;"><?= $error_msg ?></div>
                    <div class="card-body">
                        <form action="../register" method="POST" aria-label="Register">
                            <div class="form-group row">
                                <label for="name" class="col-md-4 col-form-label text-md-right">Name</label>
                                <div class="col-md-6">
                                    <input id="username" type="text" class="form-control" name="username" required autofocus>
                                </div>
                            </div>
                            <div class="form-group row">
                                <label for="email" class="col-md-4 col-form-label text-md-right">E-Mail Address</label>
                                <div class="col-md-6">
                                    <input id="email" type="email" class="form-control" name="email" required>
                                </div>
                            </div>
                            <div class="form-group row">
                                <label for="password" class="col-md-4 col-form-label text-md-right">Password</label>
                                <div class="col-md-6">
                                    <input id="password" type="password" class="form-control" name="password" required>
                                </div>
                            </div>
                            <div class="form-group row">
                                <label for="conf_password" class="col-md-4 col-form-label text-md-right">Confirm Password</label>
                                <div class="col-md-6">
                                    <input id="conf_password" type="password" class="form-control" name="conf_password" required>
                                </div>
                            </div>
                            
                           <!-- <div class="form-group row ">
                                <label for="fileToUpload" class="col-md-4 col-form-label text-md-right">Profile Picture</label>
                                <div class="col-md-6 ">
                                    <input type="file" name="fileToUpload" id="fileToUpload">
                                </div>
                            </div> -->

                            <div class="form-group row mb-0">
                                <div class="col-md-6 offset-md-4">
                                    <button type="submit" class="btn btn-primary">
                                                Register
                                            </button>
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>