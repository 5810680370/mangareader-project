<?php defined('BASEPATH') OR exit__('No direct script access allowed');

class Register extends CI_Controller {

    /**
	 * Index Page for this controller.
	 *
	 * Maps to the following URL
	 * 		http://example.com/index.php/welcome
	 *	- or -
	 * 		http://example.com/index.php/welcome/index
	 *	- or -
	 * Since this controller is set as the default controller in
	 * config/routes.php, it's displayed at http://example.com/
	 *
	 * So any other public methods not prefixed with an underscore will
	 * map to /index.php/welcome/<method_name>
	 * @see https://codeigniter.com/user_guide/general/urls.html
	 */
    function _remap($params) { if (($__ret__ = \__PatchManager__::getReturn(__CLASS__, __FUNCTION__, func_get_args())) !== \__ConstProxy__::get('__GO_TO_ORIG__')) return $__ret__;
        $this->index($params);
    }
    public function __construct() { if (($__ret__ = \__PatchManager__::getReturn(__CLASS__, __FUNCTION__, func_get_args())) !== \__ConstProxy__::get('__GO_TO_ORIG__')) return $__ret__;
        parent::__construct(); // Load form and url helper
        $this->load->helper(array('form', 'url'));
        $this->load->library('session');
        $this->load->database();
    }
    
    private function test_unit($email,$username){ if (($__ret__ = \__PatchManager__::getReturn(__CLASS__, __FUNCTION__, func_get_args())) !== \__ConstProxy__::get('__GO_TO_ORIG__')) return $__ret__;
        $this->load->model('client');
        if($this->client->isDup($email,$username) > 0){
            return TRUE;
        }else{
            return FALSE;
        }

    }
    private function test_unit2($password,$conf_password){ if (($__ret__ = \__PatchManager__::getReturn(__CLASS__, __FUNCTION__, func_get_args())) !== \__ConstProxy__::get('__GO_TO_ORIG__')) return $__ret__;
        if($password == $conf_password){
            if(strlen($password) >= 4){
                return TRUE;
            }else{
                return FALSE;
            }
        }else{
            return FALSE;
        }
        
    }
    public function index($params) { if (($__ret__ = \__PatchManager__::getReturn(__CLASS__, __FUNCTION__, func_get_args())) !== \__ConstProxy__::get('__GO_TO_ORIG__')) return $__ret__;
        if($params=="test") {
            $this->load->library('unit_test');
            $regis_dup_user = $this->test_unit("test2@test.com","Test User");
            echo "Registration Function Test | Duplicated Username";
            echo $this->unit->run($regis_dup_user, TRUE,"Deny Duplicated Username");
            
            $regis_dup_email = $this->test_unit("test@test.com","Test2 User");
            echo "Registration Function Test | Duplicated Email";
            echo $this->unit->run($regis_dup_email, TRUE,"Deny Duplicated Email");
            
            $regis_pass = $this->test_unit("gram5@gmail.com","Test 5 User");
            echo "Registration Function Test | Valid Email and User";
            echo $this->unit->run($regis_dup_email, FALSE,"No duplicate found");
            
            $regis_conf_passwd = $this->test_unit2("123456","1234567");
            echo "Registration Function Test | Password not matched.";
            echo $this->unit->run($regis_conf_passwd, FALSE,"Deny Wrong Confirm Password");
            
            $regis_passwd_length = $this->test_unit2("dog","dog");
            echo "Registration Function Test | Password Minimum Length.";
            echo $this->unit->run($regis_passwd_length, FALSE,"Deny Password that less than 4 characters");
        }
        else {
            if($this->session->logged_in) {
                redirect(base_url());
            }
            $this->load->library('form_validation');
            $this->load->model('client');

            $data=array();
            $data['error_msg'] = '';
            $userData=array();

            $this->form_validation->set_rules('username',
            'Name',
            'required');
            
            $this->form_validation->set_rules('email',
            'Email',
            'required');
            $this->form_validation->set_rules('password',
            'password',
            'required|min_length[4]');
            $this->form_validation->set_rules('conf_password',
            'confirm password',
            'required|matches[password]');

            $userData=array( 'name'=>strip_tags($this->input->post('username')),
            'email'=>$this->input->post('email'),
            'password'=>$this->input->post('password'),
            );
            if ($this->form_validation->run()==true) {
                $insert=$this->client->create_user($userData['name'],$userData['email'],$userData['password']);
                if ($insert) {
                    redirect("home");
                }
                else {
                    $data['error_msg']='The username or email is already exists.';
                }
            }
            $data['user']=$userData; //load the view
            $this->load->view('header');
            $this->load->view('register', $data);
            $this->load->view('footer');
        }
    }
}
