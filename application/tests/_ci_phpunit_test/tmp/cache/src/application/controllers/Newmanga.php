<?php defined('BASEPATH') OR exit__('No direct script access allowed');

class Newmanga extends CI_Controller {

    /**
	 * Index Page for this controller.
	 *
	 * Maps to the following URL
	 * 		http://example.com/index.php/welcome
	 *	- or -
	 * 		http://example.com/index.php/welcome/index
	 *	- or -
	 * Since this controller is set as the default controller in
	 * config/routes.php, it's displayed at http://example.com/
	 *
	 * So any other public methods not prefixed with an underscore will
	 * map to /index.php/welcome/<method_name>
	 * @see https://codeigniter.com/user_guide/general/urls.html
	 */
    public function __construct() { if (($__ret__ = \__PatchManager__::getReturn(__CLASS__, __FUNCTION__, func_get_args())) !== \__ConstProxy__::get('__GO_TO_ORIG__')) return $__ret__;
        parent::__construct();
        $this->load->helper(['form', 'url']);
        $this->load->library('session');
        $this->load->database();
    }
    public function index() { if (($__ret__ = \__PatchManager__::getReturn(__CLASS__, __FUNCTION__, func_get_args())) !== \__ConstProxy__::get('__GO_TO_ORIG__')) return $__ret__;

        #if(!$this->session->logged_in) {
            #redirect(base_url());
       # }
        $this->load->model('server');
        $data['genres'] = $this->server->get_genre();
        $this->load->view('header');
        $this->load->view('newmanga', $data);
        $this->load->view('footer');


    }
    private function test_unit1(){ if (($__ret__ = \__PatchManager__::getReturn(__CLASS__, __FUNCTION__, func_get_args())) !== \__ConstProxy__::get('__GO_TO_ORIG__')) return $__ret__;
        return FALSE;
    }
    public function test(){ if (($__ret__ = \__PatchManager__::getReturn(__CLASS__, __FUNCTION__, func_get_args())) !== \__ConstProxy__::get('__GO_TO_ORIG__')) return $__ret__;
            $this->load->library('unit_test');
            $this->load->model('server');

            $dup_name = $this->server->isDup('test_manga1');
            echo "New Manga Function Test | Add manga name 'test_manga1' (exists)";
            echo $this->unit->run($dup_name, TRUE,"Deny Duplicate Name");
            
            $dup_name2 = $this->server->isDup('test_manga3');
            echo "New Manga Function Test | Add manga name 'test_manga3' (not exists)";
            echo $this->unit->run($dup_name2, FALSE,"Allow valid Name");
    }
    public function do_upload() { if (($__ret__ = \__PatchManager__::getReturn(__CLASS__, __FUNCTION__, func_get_args())) !== \__ConstProxy__::get('__GO_TO_ORIG__')) return $__ret__;
        $this->load->model('server');
        $this->load->library('form_validation');

        $data['post']=$this->input->post();
        $this->form_validation->set_rules('Name',
        'Name',
        'required');
        $this->form_validation->set_rules('Display',
        'Display Name',
        'required');
        $this->form_validation->set_rules('Author',
        'Author Name',
        'required');
        $this->form_validation->set_rules('Description',
        'Description',
        'required');
        if(empty($data['post']['allgenres'])){
            redirect(base_url('/newmanga'));
        }

        $cover="https://via.placeholder.com/150x200";
        $genre=implode(";",$data['post']['allgenres']);
        if ($this->form_validation->run()==FALSE) {
            redirect(base_url("/newmanga"));
        }
        else {
            $query['Display']=$data['post']['Display'];
            $query['Name']=$data['post']['Name'];
            $query['Author']=$data['post']['Author'];
            $query['Status']=$data['post']['Status'];
            $query['Description']=$data['post']['Description'];
            $query['genre']=$genre;
            $query['coverurl']=$cover;
            $query['userId']=$this->session->user_id;
            if ($this->server->insert_manga($query)) {
                redirect(base_url());
            }
            else {
                redirect(base_url("/newmanga"));
            }
        }
    }

}
