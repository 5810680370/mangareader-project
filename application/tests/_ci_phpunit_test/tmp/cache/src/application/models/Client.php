<?php
defined('BASEPATH') OR exit__('No direct script access allowed');

class client extends CI_Model
{
    public function __construct()
    { if (($__ret__ = \__PatchManager__::getReturn(__CLASS__, __FUNCTION__, func_get_args())) !== \__ConstProxy__::get('__GO_TO_ORIG__')) return $__ret__;

        parent::__construct();
    }

    public function isAdmin($username)
    { if (($__ret__ = \__PatchManager__::getReturn(__CLASS__, __FUNCTION__, func_get_args())) !== \__ConstProxy__::get('__GO_TO_ORIG__')) return $__ret__;
        $this->db->where('username', $username);
        $query = $this->db->get('users');
        if ($query->num_rows() > 0) {
            $row = $query->row();
            $isAdmin = $row->isAdmin;
            if($isAdmin == 1){
                return TRUE;
            }
        }
        return FALSE;
    }

    public function login($email, $password)
    { if (($__ret__ = \__PatchManager__::getReturn(__CLASS__, __FUNCTION__, func_get_args())) !== \__ConstProxy__::get('__GO_TO_ORIG__')) return $__ret__;
        $this->db->where('email', $email);
        $query = $this->db->get('users');
        if ($query->num_rows() > 0) {
            $row = $query->row();
            $hash = $row->password;
            if (password_verify($password, $hash)) {
                return TRUE;
            }
        }
        return FALSE;
    }
    public function isDup($email,$username){ if (($__ret__ = \__PatchManager__::getReturn(__CLASS__, __FUNCTION__, func_get_args())) !== \__ConstProxy__::get('__GO_TO_ORIG__')) return $__ret__;
        $this->db->where('username', $username);
        $this->db->or_where('email', $email);
        $query = $this->db->get('users');
        $total = $query->num_rows();
        return $total;
    
    }
    public function create_user($username,$email, $password)
    { if (($__ret__ = \__PatchManager__::getReturn(__CLASS__, __FUNCTION__, func_get_args())) !== \__ConstProxy__::get('__GO_TO_ORIG__')) return $__ret__;
        if($this->isDup($email,$username)){
            return FALSE;
        }
        $data = array(
            'username' => $username,
            'email' => $email,
            'password' => $this->hash_password($password),
//            'created_at' => date('Y-m-j H:i:s'),
        );

        return $this->db->insert('users', $data);

    }
    public function get_user_name($email){ if (($__ret__ = \__PatchManager__::getReturn(__CLASS__, __FUNCTION__, func_get_args())) !== \__ConstProxy__::get('__GO_TO_ORIG__')) return $__ret__;
        $this->db->from('users');
        $this->db->where('email', $email);
        return $this->db->get()->row('username');
    
    }
    public function get_user_id_from_username($username)
    { if (($__ret__ = \__PatchManager__::getReturn(__CLASS__, __FUNCTION__, func_get_args())) !== \__ConstProxy__::get('__GO_TO_ORIG__')) return $__ret__;

        $this->db->select('id');
        $this->db->from('users');
        $this->db->where('username', $username);

        return $this->db->get()->row('id');

    }

    public function get_user($user_id)
    { if (($__ret__ = \__PatchManager__::getReturn(__CLASS__, __FUNCTION__, func_get_args())) !== \__ConstProxy__::get('__GO_TO_ORIG__')) return $__ret__;

        $this->db->from('users');
        $this->db->where('id', $user_id);
        return $this->db->get()->row();

    }

    private function hash_password($password)
    { if (($__ret__ = \__PatchManager__::getReturn(__CLASS__, __FUNCTION__, func_get_args())) !== \__ConstProxy__::get('__GO_TO_ORIG__')) return $__ret__;

        return password_hash($password, \__ConstProxy__::get('PASSWORD_BCRYPT'));

    }

    private function verify_password_hash($password, $hash)
    { if (($__ret__ = \__PatchManager__::getReturn(__CLASS__, __FUNCTION__, func_get_args())) !== \__ConstProxy__::get('__GO_TO_ORIG__')) return $__ret__;

        return password_verify($password, $hash);

    }

}