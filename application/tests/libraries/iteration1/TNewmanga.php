<?php

/**
* @group controller
*/
class queryDB extends CI_Model
{
     public function __construct(){
        parent::__construct();
         $this->load->database();
         $this->load->library('session');
         $this->load->model('server');
       
    }
}

class TNewmanga extends TestCase{
    
    public function __construct(){
        parent::__construct();
        
        
    }

    /** @test */
    public function when_information_is_null_return_false() {
        $result = new queryDB;
        $res = $result->server->get_mangas();
       
        
        $data =  [
              'Display' => '',
              'Name' => '',
              'Author' => '',
              'Status' => '',
              'Description' => '',
              'genre' => '',
              'coverurl' => '',
              'userId' => '',
             ];
             
      
        $res = $result->server->insert_manga($data);
        $this->assertFalse($res);

            
    }
    
    
    
}