<?php

/**
* @group controller
*/

class TRegister extends TestCase{
    
    /** @test */
    public function when_email_or_username_is_invalid_show_error_msg() {
        $output = $this->request(
            'POST',
            '/Register/index',
            [
                'username'  => 'hello',
                'email' => 'hello',
                'password' => '123456',
                'conf_password' => '123456',
            ]);
            $this->assertContains('The username or email is already exists.', $output);
            
    }
    
    /** @test */
    public function when_not_add_email_show_error_msg() {
        $output = $this->request(
            'POST',
            '/Register/index',
            [
                'username'  => 'hello',
                'email' => '',
                'password' => '123456',
                'conf_password' => '123456',
            ]);
            $this->assertContains('The Email field is required.', $output);
            
    }
    
    /** @test */
    public function when_not_add_username_show_error_msg() {
        $output = $this->request(
            'POST',
            '/Register/index',
            [
                'username'  => '',
                'email' => 'test@email.com',
                'password' => '123456',
                'conf_password' => '123456',
            ]);
            $this->assertContains('The Name field is required.', $output);
            
    }
    
}