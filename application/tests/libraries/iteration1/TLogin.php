<?php

/**
* @group controller
*/

class TLogin extends TestCase{
    

    
    /** @test */
    public function when_not_add_email_show_error_msg() {
        $output = $this->request(
            'POST',
            '/login',
            [
                'email' => '',
                'password' => '123456',
            ]);
           
            $this->assertContains('The Email field is required.', $output);
            
    }
    
     /** @test */
    public function when_not_add_password_show_error_msg() {
        $output = $this->request(
            'POST',
            '/Login/index',
            [
                'email' => 'test@email.com',
                'password' => '',
            ]);
            $this->assertContains('Incorrect email or password.', $output);
            
    }
    
}