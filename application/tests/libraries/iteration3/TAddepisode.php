<?php
class queryDB extends CI_Model
{
     public function __construct(){
        parent::__construct();
         $this->load->database();
         $this->load->library('session');
         $this->load->model('server');
       
    }
}
class TAddepisode extends TestCase{
    
    /** @test */
     public function when_add_Name_only_return_true() {
        $result = new queryDB;
        $res = $result->server->get_mangas();
       
        
        $data =  [
            'Name' => "Hello",
            'images' => ''
             ];
             
      
        $res = $result->server->insert_episode($data);
        $this->assertTrue($res);

            
    }
    
    /** @test */
     public function when_not_add_Name_only_return_false() {
        $result = new queryDB;
        $res = $result->server->get_mangas();
       
        
        $data =  [
            'Name' => "",
            'images' => ''
             ];
             
      
        $res = $result->server->insert_episode($data);
        $this->assertFalse($res);

            
    }
    

}