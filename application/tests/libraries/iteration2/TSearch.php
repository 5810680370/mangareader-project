<?php

/**
* @group controller
*/

class TSearch extends TestCase{
    
    /** @test */
    public function when_not_add_keywords_in_search_box_show_all_manga(){
        $output = $this->request(
                'POST',
                '/Search/index',
                [
                    'name' => '',
                ]
            );
        $this->assertContains('Found: 8 result(s).',$output);
    }
    
    /** @test */
    public function when_not_mangas_match_keyword_no_manga_show_on_on_the_page(){
        $output = $this->request(
                'POST',
                '/Search/index',
                [
                    'name' => 'a!@#!$!@#',
                ]
            );
        $this->assertContains('Found: 0 result',$output);
    }
    
}