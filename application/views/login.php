    <div class="container">
        <br>
        <div class="row justify-content-center">
            <div class="col-md-8">
                <div class="card">
                    <div class="card-header">Login</div>
                        <?php echo validation_errors('<br><div style="color:red; text-align: center;">', '</div>'); ?>
                    <div class="card-body">
                        <form action="../login" method="POST" aria-label="Login">


                            <div class="form-group row">
                                <label for="email" class="col-md-4 col-form-label text-md-right">E-Mail Address</label>

                                <div class="col-md-6">
                                    <input id="email" type="email" class="form-control" name="email" required>


                                </div>
                            </div>

                            <div class="form-group row">
                                <label for="password" class="col-md-4 col-form-label text-md-right">Password</label>

                                <div class="col-md-6">
                                    <input id="password" type="password" class="form-control" name="password" required>


                                </div>
                            </div>


                            <div class="form-group row mb-0">
                                <div class="col-md-6 offset-md-4 ">
                                    <div class="row">
                                        <div class="col-md-6">
                                            <button type="submit" class="btn btn-primary">
                                                        Login
                                                    </button>
                                        </div>
                                        <div class="col-md-6">
                                            <button type="reset" class="btn btn-warning">
                                                        Reset
                                                    </button>
                                        </div>
                                    </div>
                                </div>


                            </div>
                            <div class="form-group row mb-0">
                                <div class="col-md-6 offset-md-4">

                                </div>
                            </div>

                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>

