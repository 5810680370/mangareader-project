<div class="container">
    <br>
    <div class="row">
        Found: <?= sizeof($mangas) ?> result(s). [keyword: "<?= $name ?>"]
    </div>
    <br>
    <div class="row">
<?php if (sizeof($mangas) > 0) : ?>
                <?php foreach ($mangas as $m): ?>
                    <div class="card" style="padding: 5px; text-align: center;">
                        <a href="<?= base_url('manga/') . $m['Name'] ?>"><img
                                    src="<?= $m['coverurl'] ?>" width="150px"
                                    height="200px"></a>
                        <?php
                        $string = $m['Display'];
                        if (strlen($string) >= 16)
                            $short = substr($string, 0, 16) . "...";
                        else
                            $short = $string;
                        ?>
                        <a href="<?= base_url('manga/') . $m['Name'] ?>"><p><?= $short ?></p></a>
                    </div>
                <?php endforeach; endif; ?>
    </div>
</div>