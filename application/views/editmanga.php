<?php echo form_open_multipart('edit/updater'); ?>
<div class="container">
    <br>
    <div class="card">
        <div class="card-header" style="text-align: center;">
            Manga
            <div style="float: right;">
                <button type="reset" class="fa fa-times" style="border: 0; background: transparent; outline: none;"
                        onclick="return confirm('Are you sure you want to reset?')">
                </button>
                <button type="submit" class="fa fa-check" style="border: 0; background: transparent; outline: none;"
                        onclick="return confirm('Are you sure?')">
                </button>
            </div>
        </div>
        <div class="card-body">
            <div class="row">
                <div class="col-md-3" style="text-align: center;">
                    <div class="container" style="background-color: lightblue; color:white; min-height:300px;">
                        Picture
                    </div>
                    <br>
                    <!--<a href="#" class="btn btn-success" type="file">Upload</a>-->
                    <input type="hidden" name="Id" value="<?php echo($mangas['Id']);?>">
                </div>
                <div class="col-md-9">
                    <div class="container">
                        
                        <div class="row">
                            <div class="col-md-9">
                              
                                    <h5><label id="Author">Display Name:</label></h5>
                                    <input required id="Display" class="form-control" type="text" name="Display"
                                           placeholder="ex. Knights & Magic" value="<?php echo($mangas['Display']); ?>">
                            </div>
                        </div>
                        <br>
                        <div class="row">
                            <div class="col-md-9">
                                <h5><label id="Author">Name ID:</label></h5>
                                <input required id="Name" class="form-control" type="text" name="Name"
                                       placeholder="ex. knights_magic" value="<?php echo($mangas['Name']); ?>">
                            </div>
                        </div>
                        <br>
                        <div class="row">
                            <div class="col-md-9">
                                <h5><label id="Author">Author(s):</label></h5>
                                <input required id="Author" class="form-control" type="text" name="Author"
                                       placeholder="ex. Amazake No Hisago" value="<?php echo($mangas['Author']); ?>">
                            </div>
                        </div>
                        <br>
                        <div class="row">
                            <div class="col-md-9">
                                <h5><label id="Author">Status:</label></h5>
                                <input type="radio" id='ongoing' name="Status" value="0" <?php echo($mangas['Status']==0 ? "checked":""); ?>><label id="ongoing">Ongoing </label>
                                <input id='complete' type="radio" name="Status" value="1" <?php echo($mangas['Status']==1 ? "checked":""); ?>><label
                                        id="complete">Completed</label>
                            </div>
                        </div>
                        <br>
                        <div class="row">
                            <div class="col-md-12">
                                <h5><label id="Description">Description:</label></h5>
                                <textarea required id="Description" de
                                          type="text"
                                          class="form-control"
                                          name="Description"
                                          style="min-height: 150px"
                                          placeholder=""><?php echo($mangas['Description']); ?></textarea>
                            </div>
                        </div>
                        <br>
                        <div class="row">
                            <div class="col-md-12">
                                <h5><label>Genre:</label></h5>
                                <select name="allgenres[]" class="selectpicker" multiple>
                                    <?php foreach ($genres as $g): ?>
                                        <option value="<?= $g['Id'] ?>" <?php echo($mangas['genre']==$g['Id'] ? "selected":""); ?> ><?= $g['Name'] ?></option>
                                    <?php endforeach; ?>
                                </select>
                                
                                </form>
                            </div>
                        </div>
                        <br>

                    </div>
                </div>

            </div>
        </div>
    </div>
</div>