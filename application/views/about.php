<div class="container">
	<br>
	<div class="card">
		<div class="card-header">
			About Us
		</div>
		<div class="card-body">
			<div class="row">
				<div class="col-md-6">
					<div class="card">
						<div class="card-header">
							Project Members
						</div>
						<div class="card-body" style="min-height: 300px;">
							<p>ศิรกานต์ อันประนิตย์ 5810680370</p>
							<p>ธนภัทร วงธ์แหวน 5910613297</p>
							<p>บุญณัฐ พึ่งแย้มศรวล 5910613305</p>
							<p>พรหมสุรินท์ พุทธรรมวงศ์ 5910613313</p>
						</div>

					</div>
				</div>
				<div class="col-md-6">
					<div class="card">
						<div class="card-header">
							Requirement
						</div>
						<div class="card-body" style="min-height: 300px;">
							<p>∙ สามารถสมัครสมาชิกและเข้าสู่ระบบได้</p>
							<p>∙ สามารถเพิ่ม/แก้ไขมังงะ(Manga)ใหม่ได้</p>
							<p>∙ สามารถเพิ่ม/แก้ไขตอน(Chapter)ให้กับมังงะนั้นๆได้</p>
							<p>∙ สามารถค้นหามังงะได้โดยใช้ Keyword ชื่อเรื่อง</p>
						</div>

					</div>
				</div>
			</div>
			<br>
			<div class="row">
				<div class="col-md-12">
					<div class="card">
						<div class="card-header">
							Features
						</div>
						<div class="card-body" style="min-height: 300px;">
							<div class="row">
								<div class="col-md-12">
									<div class="card">
										<div class="card-header">
											Status Color
										</div>
										<div class="card-body" ">
										<div class="row ">
								<div class="col-md-6 ">
									<div class="card ">
										<div class = "card-header " style="background-color: lightgreen; ">
											   Completed
										</div>
									</div>
								</div>
								<div class="col-md-6 ">
									<div class="card ">
										<div class = "card-header " style="background-color: lightgrey; ">
											   In Development
										</div>
									</div>
								</div>
								</div>
										</div>
									</div>
								</div>
							</div>
							<br>
							<div class="row ">
								<div class="col-md-6 ">
									<div class="card ">
										<div class = "card-header ">
											   Registration
										</div>
										<div class="card-body " style="background-color:lightgreen; min-height: 150px; ">
										<p>∙ As a guest<br>
										∙ So that I can become more than just a reader.<br>
										∙ I want to fill in my information to register as a member.
										</p>
										</div>
									</div>
								</div>
								<div class="col-md-6 ">
									<div class="card ">
										<div class = "card-header ">
											   Login
										</div>
										<div class="card-body " style="background-color:lightgreen; min-height: 150px; ">
										<p>∙ As a guest<br>
										∙ So that I can start sharing my own favorite manga with my follower.<br>
										∙ I want to log in as a member with the username I registered
										</p>
										</div>
									</div>
								</div>
							</div>
							<br>
							<div class="row ">
								<div class="col-md-6 ">
									<div class="card ">
										<div class = "card-header ">
											   Add New Manga
										</div>
										<div class="card-body " style="background-color:lightgreen; min-height: 150px; ">
										<p>∙ As a member<br>
										∙ So that I can add a new manga that not exists in the database so that other readers can read it.<br>
										∙ I want to add a new manga to mangareader database
										</p>
										</div>
									</div>
								</div>
								<div class="col-md-6 ">
									<div class="card ">
										<div class = "card-header ">
											   Add New Episode
										</div>
										<div class="card-body " style="background-color:lightgreen; min-height: 150px; ">
										<p>∙ As an author<br>
										∙ So that I can add a new episode on manga for everyone who following my manga.<br>
										∙ I want to add a new episode to mangareader database
										</p>
										</div>
									</div>
								</div>
							</div>
							<br>
							<div class="row ">
								<div class="col-md-6 ">
									<div class="card ">
										<div class = "card-header ">
											   Edit Manga
										</div>
										<div class="card-body " style="background-color:lightgreen; min-height: 150px; ">
										<p>∙ As an author<br>
										∙ So that I can correct my manga information<br>
										∙ I want to modify manga information and upload to mangareader database
										</p>
										</div>
									</div>
								</div>
								<div class="col-md-6 ">
									<div class="card ">
										<div class = "card-header ">
											   Delete Manga
										</div>
										<div class="card-body " style="background-color:lightgreen; min-height: 150px; ">
										<p>∙ As an author<br>
										∙ So that I can delete episode on manga because it is a false episode.<br>
										∙ I want to remove a manga’s episode from mangareader database.
										</p>
										</div>
									</div>
								</div>
							</div>
							<br>
							<div class="row ">
								<div class="col-md-6 ">
									<div class="card ">
										<div class = "card-header ">
											   Delete Episode
										</div>
										<div class="card-body " style="background-color:lightgreen; min-height: 150px; ">
										<p>∙ As an author<br>
										∙ So that I can delete a manga that have incorrect information or abandoned.<br>
										∙ I want to remove a manga's data from mangareader database.
										</p>
										</div>
									</div>
								</div>
								<div class="col-md-6 ">
									<div class="card ">
										<div class = "card-header ">
											   Manga Searching
										</div>
										<div class="card-body " style="background-color:lightgreen; min-height: 150px; ">
										<p>∙ As a guest and member<br>
										∙ So that I can find a manga by name or genre.<br>
										∙ I want to search a manga that matched my search criteria.
										</p>
										</div>
									</div>
								</div>
							</div>
							<br>
							<div class="row ">
								<div class="col-md-6 ">
									<div class="card ">
										<div class = "card-header ">
											   Rating
										</div>
										<div class="card-body " style="background-color:lightgreen; min-height: 150px; ">
										<p>∙ As a member<br>
										∙ So that I can give a good manga my rating to share that this manga is good or bad to other readers.<br>
										∙ I want to rate a manga between 1 to 5 stars.
										</p>
										</div>
									</div>
								</div>
								<div class="col-md-6 ">
									<div class="card ">
										<div class = "card-header ">
											   Update Rating
										</div>
										<div class="card-body " style="background-color:lightgreen; min-height: 150px; ">
										<p>∙ As a member<br>
										∙ So that I can change the rating of manga I rated before because it was better or worse when I continue reading further.<br>
										∙ I want to change the rating star of a selected manga.
										</p>
										</div>
									</div>
								</div>
							</div>
						</div>
                    </div>
                </div>
            </div>
			<br>
			<div class="row ">
                <div class="col-md-12 ">
                    <div class="card ">
					    <div class = "card-header ">
                            Poster
                        </div>
                        <div class="card-body " style="text-align: center;min-height: 300px; ">
							<img src="<?= base_url(); ?>img/poster.jpg"></a>
                        </div>

                    </div>
                </div>
            </div>
			<br>
			<div class="row ">
                <div class="col-md-12 ">
				
                    <div class="card ">
					    <div class = "card-header ">
                            ER-Diagram
                        </div>
                        <div class="card-body " style="text-align: center;min-height: 300px; ">
							<a href="https://ibb.co/gdDRDV "><img src="https://preview.ibb.co/mYAj0A/8854296203606.jpg " alt="8854296203606 " border="0 "></a>
                        </div>

                    </div>
                </div>
            </div>
			<br>
			<div class="row ">
                <div class="col-md-12 ">
				
                    <div class="card ">
					    <div class = "card-header ">
                            Iteration 1
                        </div>
                        <br>
                        <div class="card-body " style="min-height: 300px; ">
							<div class="row ">
								<div class="col-md-6 ">
									<div class="card ">
										<div class = "card-header ">
											   Registration
										</div>
										<div class="card-body " style="background-color:lightgreen; min-height: 150px; ">
										<p>∙ As a guest<br>
										∙ So that I can become more than just a reader.<br>
										∙ I want to fill in my information to register as a member.
										</p>
										</div>
									</div>
								</div>
								<div class="col-md-6 ">
									<div class="card ">
										<div class = "card-header ">
											   Login
										</div>
										<div class="card-body " style="background-color:lightgreen; min-height: 150px; ">
										<p>∙ As a guest<br>
										∙ So that I can start sharing my own favorite manga with my follower.<br>
										∙ I want to log in as a member with the username I registered
										</p>
										</div>
									</div>
								</div>
							</div>
							<br>
							<div class="row ">
								<div class="col-md-6 ">
									<div class="card ">
										<div class = "card-header ">
											   Add New Manga
										</div>
										<div class="card-body " style="background-color:lightgreen; min-height: 150px; ">
										<p>∙ As a member<br>
										∙ So that I can add a new manga that not exists in the database so that other readers can read it.<br>
										∙ I want to add a new manga to mangareader database<br>
										</p>
										</div>
									</div>
								</div>
							</div>
							<br>
							</div>
                    </div>
                </div>
            </div>
			<br>
			<div class="row ">
                <div class="col-md-12 ">
				
                    <div class="card ">
					    <div class = "card-header ">
                            Iteration 2
                        </div>
                        <div class="card-body " style="min-height: 300px; ">
							<div class="row ">
								<div class="col-md-6 ">
									<div class="card ">
										<div class = "card-header ">
											   Rating
										</div>
										<div class="card-body " style="background-color:lightgreen; min-height: 150px; ">
										<p>∙ As a member<br>
										∙ So that I can give a good manga my rating to share that this manga is good or bad to other readers.<br>
										∙ I want to rate a manga between 1 to 5 stars.
										</p>
										</div>
									</div>
								</div>
								<div class="col-md-6 ">
									<div class="card ">
										<div class = "card-header ">
											   Update Rating
										</div>
										<div class="card-body " style="background-color:lightgreen; min-height: 150px; ">
										<p>∙ As a member<br>
										∙ So that I can change the rating of manga I rated before because it was better or worse when I continue reading further.<br>
										∙ I want to change the rating star of a selected manga.
										</p>
										</div>
									</div>
								</div>
							</div>
							<br>
							<div class="row ">
								<div class="col-md-6 ">
									<div class="card ">
										<div class = "card-header ">
											   Manga Searching
										</div>
										<div class="card-body " style="background-color:lightgreen; min-height: 150px; ">
										<p>∙ As a guest and member<br>
										∙ So that I can find a manga by name or genre.<br>
										∙ I want to search a manga that matched my search criteria.
										</p>
										</div>
									</div>
								</div>
								<br>
								<div class="col-md-6 ">
									<div class="card ">
										<div class = "card-header ">
											   Delete Manga
										</div>
										<div class="card-body " style="background-color:lightgreen; min-height: 150px; ">
										<p>∙ As an author<br>
										∙ So that I can delete episode on manga because it is a false episode.<br>
										∙ I want to remove a manga’s episode from mangareader database.
										</p>
										</div>
									</div>
								</div>
							</div>
							<br>
						</div>
                    </div>
                </div>
            </div>
			<br>
			<div class="row ">
                <div class="col-md-12 ">
				
                    <div class="card ">
					    <div class = "card-header ">
                            Iteration 3
                        </div>
                        <div class="card-body " style="min-height: 300px; ">
							<div class="row ">
								<div class="col-md-6 ">
									<div class="card ">
										<div class = "card-header ">
											   Add New Episode
										</div>
										<div class="card-body " style="background-color:lightgreen; min-height: 150px; ">
										<p>∙ As an author<br>
										∙ So that I can add a new episode on manga for everyone who following my manga.<br>
										∙ I want to add a new episode to mangareader database
										</p>
										</div>
									</div>
								</div>
								<br>
								<div class="col-md-6 ">
									<div class="card ">
										<div class = "card-header ">
											   Edit Manga
										</div>
										<div class="card-body " style="background-color:lightgreen; min-height: 150px; ">
										<p>∙ As an author<br>
										∙ So that I can correct my manga information<br>
										∙ I want to modify manga information and upload to mangareader database
										</p>
										</div>
									</div>
								</div>
							</div>
							<br>
							<div class="row ">
								<div class="col-md-6 ">
									<div class="card ">
										<div class = "card-header ">
											   Delete Episode
										</div>
										<div class="card-body " style="background-color:lightgreen; min-height: 150px; ">
										<p>∙ As an author<br>
										∙ So that I can delete a manga that have incorrect information or abandoned.<br>
										∙ I want to remove a manga's data from mangareader database.
										</p>
										</div>
									</div>
								</div>
							</div>
						</div>
                    </div>
                </div>
            </div>

			<br>
			<div class="row ">
                <div class="col-md-12 ">
				
                    <div class="card ">
					    <div class = "card-header ">
                            Project Proposal
                        </div>
                        <div class="card-body " style="text-align:center; ">
                        	<a class="btn btn-primary " href="https://drive.google.com/file/d/1Ip4c-RatPuZx__Kn4k2sw5un14ASQ-Yk/view?usp=sharing " role="button ">Google Drive</a>
                        </div>

                    </div>
                </div>
            </div>
			<br>
			<div class="row ">
                <div class="col-md-12 ">
				
                    <div class="card ">
					    <div class = "card-header ">
                            Youtube Iteration 0
                        </div>
                        <div class="card-body " style="text-align:center;min-height: 300px; ">
                        	<iframe width="560 " height="315 " src="https://www.youtube.com/embed/AjhRA3LuXp8 " frameborder="0 " allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture " allowfullscreen></iframe>
                        </div>

                    </div>
                    <br>
                    <div class="card ">
					    <div class = "card-header ">
                            Youtube Final Presentation
                        </div>
                        <div class="card-body " style="text-align:center;min-height: 300px; ">
                        	<iframe width="560" height="315" src="https://www.youtube.com/embed/MOB4ahsdCNU" frameborder="0" allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>
                        </div>

                    </div>
                </div>
            </div>
            

			<br>
			
			<div class="row ">
                <div class="col-md-12 ">
				
                    <div class="card ">
					    <div class = "card-header ">
                            Link
                        </div>
                        <div class="card-body " style="min-height: 300px; ">
                        <p>Pivotal Tracker <a href="https://www.pivotaltracker.com/n/projects/2216020 ">https://www.pivotaltracker.com/n/projects/2216020</a></p>
                        <p>Bitbucket Repo <a href="https://bitbucket.org/5810680370/mangareader-project/src/gram/ ">https://bitbucket.org/5810680370/mangareader-project/src/gram/</a></p>
                        <p>Youtube Iteration0 <a href="https://www.youtube.com/watch?v=AjhRA3LuXp8">https://www.youtube.com/watch?v=AjhRA3LuXp8</a></p>
                        <p>Youtube Final Presentation <a href="https://www.youtube.com/watch?v=MOB4ahsdCNU">https://www.youtube.com/watch?v=MOB4ahsdCNU</a></p>
                        
                        </div>

                    </div>
                </div>
            </div>
			<br>
        </div>
    </div>
</div>
