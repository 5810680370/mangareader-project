
<div class="container">
    <br>
    <div class="row justify-content-center">
        <div class="col-md-8">
            <div class="card">
                <div class="card-header">New Chapter</div>

                <div class="card-body">
                    <?php echo form_open_multipart('newchap/insert'); ?>


                        <div class="form-group row">
                            <label for="drop1" class="col-md-4 col-form-label text-md-right">Manga</label>

                            <div class="col-md-6">
                                <div class="dropdown" id="drop1">
                                    <select name="manga" required class="form-control">
                                        <!--
                                        <option disabled selected>Please Select Manga</option>
                                        <option value="manga1">Manga 1</option>
                                        <option value="manga2">Manga 2</option>
                                        <option value="manga3">Manga 3</option>
                                        -->
                                        <option disabled selected>Please Select Manga</option>
                                        <?php foreach ($mangas as $m): ?>
                                        <?php if (($this->session->user_id == $m['userId'])): ?>
                                            <option value="<?= $m['Name'] ?>"><?= $m['Display'] ?></option>
                                        <?php endif; endforeach; ?>
                                    </select>
                                </div>


                            </div>
                        </div>
                        <div class="form-group row">
                            <label for="name" class="col-md-4 col-form-label text-md-right">Episode Name</label>

                            <div class="col-md-6">
                                <input id="name" type="text" class="form-control" name="name" required>

                            </div>
                        </div>
                    <div class="form-group row">
                        <label for="episodeurl" class="col-md-4 col-form-label text-md-right">Episode URL</label>

                        <div class="col-md-6">
                            <input id="episodeurl" type="url" class="form-control" name="episodeurl" required>


                        </div>
                    </div>
                    <div class="form-group row">
                        <label for="picture1" class="col-md-4 col-form-label text-md-right">Picture URL1</label>

                        <div class="col-md-6">
                            <input id="picture1" type="url" class="form-control" name="picture1" >


                        </div>
                    </div>
                    <div class="form-group row">
                        <label for="picture2" class="col-md-4 col-form-label text-md-right">Picture URL2</label>

                        <div class="col-md-6">
                            <input id="picture2" type="url" class="form-control" name="picture2" >


                        </div>
                    </div>
                    <div class="form-group row">
                        <label for="picture3" class="col-md-4 col-form-label text-md-right">Picture URL3</label>

                        <div class="col-md-6">
                            <input id="picture3" type="url" class="form-control" name="picture3" >


                        </div>
                    </div>

                        <div class="form-group row mb-0">
                            <div class="col-md-6 offset-md-4">
                                <button type="submit" class="btn btn-primary">
                                    Submit
                                </button>
                            </div>
                        </div>


                    </form>
                </div>
            </div>
        </div>
    </div>
</div>


