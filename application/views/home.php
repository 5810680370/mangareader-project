<?php
//    die(var_dump(scandir(FCPATH . 'public/upload/knights_magic/1')));
if ($mangas) : ?>
    <div class="container">
        <br>
        <div class="card">
            <div class="card-header">
                Available Mangas
            </div>
            <div class="card-body">
                <div class="row">
                <?php foreach ($mangas as $m): ?>
                <div class="col-md-2">
                    <a href="<?= 'manga/' . $m['Name'] ?>"><img src="https://via.placeholder.com/150x200" width="150px" height="200px"></a>

                    <?php
                $string = $m['Display'];
                if (strlen($string) >= 20)
                    $short = substr($string, 0, 20) . "...";
                else
                    $short = $string;
                ?>
                        <p>
                            <a href="<?= base_url('manga/' . $m["Name"] . '/'  . '/') ?>">
                                <?= $short . " " ?>
                            </a>
                        </p>
                </div>
                <?php endforeach; ?>
                </div>
            </div>
        </div>
     </div>
    <?php else: ?>
    <div class="container">
        <br>
        <div class="card">
            <div class="card-header">
                Nothing Here. Add some new manga!.
            </div>

        </div>
    </div>
    <?php endif; ?>
