<div class="container">
    <br>
    <div class="card">
        <div class="card-header" style="text-align: center;">
            <?=  $manga['Display']; ?>
            <?php if($manga['userId'] == $this->session->user_id):?>
            <div style="float: right">
                <?php echo form_open_multipart('delete/'.$manga['Name']); ?>
                <button type="submit" class="fa fa-times" style="border: 0; background: transparent; outline: none;"
                    onclick="return confirm('Are you sure you want to delete this?')">
            </button>
            </form>
            </div>
            <?php endif; ?>
        </div>
        <div class="card-body">
            <div class="row">
                <div class="col-md-2">
                       <img src="<?= $manga['coverurl'];?>" width=150 height=200>
                </div>
                <div class="col-md-10">
                    <div class="container">
                        <div class="row">
                            <div class="col-md-9">
                                <?php echo form_open_multipart('manga/'.$manga['Name']); ?>
                                    <fieldset class="rating">
                                        <p>Rating:</p>
                                        <input type="radio" id="star5" name="rating" value="5" <?php if($star == 5) echo 'checked'?> onchange="myFunction();"/><label for="star5">5 stars</label>
                                        <input type="radio" id="star4" name="rating" value="4" <?php if($star == 4) echo 'checked'?> onchange="myFunction();" /><label for="star4">4 stars</label>
                                        <input type="radio" id="star3" name="rating" value="3" <?php if($star == 3) echo 'checked'?> onchange="myFunction();" /><label for="star3">3 stars</label>
                                        <input type="radio" id="star2" name="rating" value="2" <?php if($star == 2) echo 'checked'?> onchange="myFunction();" /><label for="star2">2 stars</label>
                                        <input type="radio" id="star1" name="rating" value="1" <?php if($star == 1) echo 'checked'?> onchange="myFunction();" /><label for="star1">1 star</label>
                                        <?php if($this->session->logged_in):?>
                                        <button id="hidden1" type="submit" style="display: none;"></button>
                                        <?php endif;?>
                                    </fieldset>
                                    <script>
                                        function myFunction() {
                                            document.getElementById('hidden1').click();
                                        }
                                    </script>
                                </form>
                                <?= "Average Rating: ".$total['avg']; ?>
                            </div>
                        </div>
                        <br>
                        <div class="row">
                            <div class="col-md-9">
                                <p>Author(s):</p>
                                <p style="color: gray;"><?=  $manga['Author']; ?></p>
                            </div>
                <?php if ($this->session->logged_in && $this->session->user_id == $manga['userId']) : ?>
                            <div class="col-md-3">
                                <a href="<?= base_url('edit/') . $manga['Name'] ?>" class="btn btn-primary">Edit</a>
                            </div>
                <?php endif; ?>
                        </div>
                        <br>
                        <div class="row">
                            <div class="col-md-12">
                                    <p>Description:</p>
                                    <div style="padding: 5px; border: 1px solid black; border-radius: 5px">
                                        <p style="color: gray;"><?= $manga['Description']; ?></p>
                                    </div>
                            </div>


                        </div>
                        <br>
                        <div class="row">
                            <div class="col-md-12">
                                
                                <?php foreach ($genre as $g): ?>
                                <a href="#" class="btn btn-primary"><?= $g['Name']?></a>
                                <?php endforeach;?>
                            </div>
                        </div>
                    </div>
                </div>

            </div>
            <div class="row">
            <br>
            <table style="width: 100%; margin: 0 10%;">
            <tr>
                <th style="text-align: center;">Manga Chapter List</th>
                <th style="text-align: right;">Date</th>
            </tr>
                <?php if(!empty($chapters)): ?>
                <?php foreach ($chapters as $c): ?>
                    <tr>
                        <td>
                            <a href="<?= base_url('read/' . $c["Manga_NameId"] . '/' . $c["Id"] . '/') ?>">
                                <?= $c['Manga_Name'] ?> : <?= $c['Name'] ?>
                            </a>
                            <?php if ($manga['userId'] == $this->session->user_id) : ?>
                                <div style="float: right;">
                                    <a href="<?= base_url('delete/') . $manga['Name'] . "/" . $c['Id'] ?>"
                                       onclick="return confirm('Delete <?= $manga['Display'] ?> : <?= $c['Name'] ?>?')">
                                        <div class="fa fa-trash"></div>
                                    </a>
                                </div>
                            <?php endif; ?>
                        </td>
                        <?php

                        //Our dates
                        $date1 = date('Y-m-d H:i:s');
                        $date2 = $c['Date'];

                        //Convert them to timestamps.
                        $date1Timestamp = strtotime($date1);
                        $date2Timestamp = strtotime($date2);

                        //Calculate the difference.
                        $difference = $date1Timestamp - $date2Timestamp;
                        $min = ($difference / 60);
                        $hr = ($difference / 60 / 60);
                        $day = ($difference / 60 / 60 / 24);
                        $datetime = date("F d, Y H:i:s", strtotime($date2));
                        if ($hr < 1) {
                            $a = floor($min);
                            $datetime = "$a minutes ago.";
                        } else if ($day < 1) {
                            $a = floor($hr);
                            $datetime = "$a hours ago.";
                        } else if ($day < 8) {
                            $a = floor($day);
                            $datetime = "$a days ago.";
                        }
                        ?>
                        <td style="text-align: right;"><?= $datetime ?></td>
                    </tr>
                <?php endforeach; endif;?>
        </table>
            </div>
        </div>
    </div>
</div>