<div class="container">
    <br>
    <div class="row justify-content-center">
        <div class="col-md-8">
            <div class="card">
                <div class="card-header">New Chapter</div>

                <div class="card-body">
                    <?php echo form_open_multipart('newchap/insert'); ?>

                    <!---->
                    <div class="form-group row">
                        <label for="drop1" class="col-md-4 col-form-label text-md-right">Manga</label>

                        <div class="col-md-6">
                            <div class="dropdown" id="drop1">
                                <select name="Manga_Id" required class="form-control">
                                    <option disabled selected>Please Select Manga</option>
                                        <?php foreach ($mangas as $m): ?>
                                        <?php if (($this->session->user_id == $m['userId'])): ?>
                                            <option value="<?= $m['Id'] ?>"><?= $m['Display'] ?></option>
                                        <?php endif; endforeach; ?>
                                </select>
                            </div>


                        </div>
                    </div>
                    <div class="form-group row">
                        <label for="Name" class="col-md-4 col-form-label text-md-right">Episode Name</label>

                        <div class="col-md-6">
                            <input id="Name" type="text" class="form-control" name="Name" required>


                        </div>
                    </div>


                    <div class="form-group row ">
                        <label for="images" class="col-md-4 col-form-label text-md-right">Picture</label>
                        <div class="col-md-6 ">
                          
                            <div class="inputs" id="imagelist">
                                <button class="add_form_field">Add New URL &nbsp; <span
                                            style="font-size:16px; font-weight:bold;">+ </span></button>
                                <br>
                                <input type="text" name="images[]" class="field" 
                                /></div>

                        </div>
                    </div>

                    <div class="form-group row mb-0">
                        <div class="col-md-6 offset-md-4">
                            <button type="submit" class="btn btn-primary">
                                Submit
                            </button>
                        </div>
                    </div>


                    </form>
                </div>
            </div>
        </div>
    </div>
</div>

<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.1.0/jquery.min.js"></script>
<script>
    function addimages() {
        var x = document.getElementById('imagelist');
        var input = document.createElement("input");
        input.setAttribute("type", "text");
        input.setAttribute("name", "images[]");
        input.setAttribute("placeholder", "test");
        x.appendChild(input);

    }
</script>
<script>
    $(document).ready(function () {
        var max_fields = 10;
        var wrapper = $(".inputs");
        var add_button = $(".add_form_field");

        var x = 1;
        $(add_button).click(function (e) {
            e.preventDefault();
            if (x < max_fields) {
                x++;
                $(wrapper).append('<div><input type="text" name="images[]"/><a href="#" class="delete">Delete</a></div>'); //add input box
            }
            else {
                alert('You Reached the limits')
            }
        });

        $(wrapper).on("click", ".delete", function (e) {
            e.preventDefault();
            $(this).parent('div').remove();
            x--;
        })
    });
</script>