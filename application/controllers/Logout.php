<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

class Logout extends CI_Controller
{
    public function __construct()
    {
        parent::__construct();

        // Load form and url helper
        $this->load->helper(array('form','url'));
        $this->load->library('session');
        $this->load->database();
    }
    public function index()
    {
        if(!$this->session->logged_in){
            redirect(base_url());
        }
        // Load url helper
        $this->load->helper('url');
        $this->session->sess_destroy();
        redirect('/');
    }
}