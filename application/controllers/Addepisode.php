<?php defined('BASEPATH') OR exit('No direct script access allowed');

class Addepisode extends CI_Controller
{

    /**
     * Index Page for this controller.
     *
     * Maps to the following URL
     *        http://example.com/index.php/welcome
     *    - or -
     *        http://example.com/index.php/welcome/index
     *    - or -
     * Since this controller is set as the default controller in
     * config/routes.php, it's displayed at http://example.com/
     *
     * So any other public methods not prefixed with an underscore will
     * map to /index.php/welcome/<method_name>
     * @see https://codeigniter.com/user_guide/general/urls.html
     */
    public function __construct()
    {
        parent::__construct();
        $this->load->helper(['form', 'url']);
        $this->load->library('session');
        $this->load->database();
    }
    public function index() {

        #if(!$this->session->logged_in) {
        #redirect(base_url());
        # }
        $this->load->model('server');

        $data['mangas'] = $this->server->get_user_mangas($this->session->user_id);
        die(var_dump($this->session->user_id));
        $this->load->view('header');
        $this->load->view('addepisode', $data);
        $this->load->view('footer');


    }

}