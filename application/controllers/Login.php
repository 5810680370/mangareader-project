<?php defined('BASEPATH') OR exit('No direct script access allowed');

class Login extends CI_Controller {

    /**
	 * Index Page for this controller.
	 *
	 * Maps to the following URL
	 * 		http://example.com/index.php/welcome
	 *	- or -
	 * 		http://example.com/index.php/welcome/index
	 *	- or -
	 * Since this controller is set as the default controller in
	 * config/routes.php, it's displayed at http://example.com/
	 *
	 * So any other public methods not prefixed with an underscore will
	 * map to /index.php/welcome/<method_name>
	 * @see https://codeigniter.com/user_guide/general/urls.html
	 */
    function _remap($params) {
        $this->index($params);
    }
    public function __construct() {
        parent::__construct(); // Load form and url helper
        $this->load->helper(array('form', 'url'));
        $this->load->library('session');
        $this->load->database();
    }
    
    private function test_unit($email,$password){
        $this->load->model('client');
        if ($this->client->login($email, $password)) {
            return TRUE;
        }else{
            return FALSE;
        }
    }
    public function index($params) {
        if($params=="test") {
            $this->load->library('unit_test');
            
            $login_good = $this->test_unit("test@test.com","123456");
            echo "Login Function Test (user: test@test.com | passwd: 123456)";
            echo $this->unit->run($login_good, TRUE,"Allow Valid User");
            
            $login_bad = $this->test_unit("test2@test.com","123456");
            echo "Login Function Test (user: test2@test.com | passwd: 123456)";
            echo $this->unit->run($login_bad, FALSE,"Deny Invalid User");
        }
        else {
            if($this->session->logged_in) {
                redirect('home');
            } // Load form validation
            $this->load->library('form_validation');

            $this->form_validation->set_rules('email', 'Email', 'trim|required');
            $this->form_validation->set_rules('password', 'Password', 'trim|required|callback_check_database');

            if ($this->form_validation->run()==FALSE) {
                $this->load->view('header');
                $this->load->view('login');
                $this->load->view('footer');
            }
            else {
                redirect('home', 'refresh');
            }
        }
    }

    public function check_database($password) {
        $this->load->model('client');

        $email=$this->input->post('email');


        if ($this->client->login($email, $password)) {

            $this->session->set_userdata('email', $email);
            $this->session->set_userdata('username', $this->client->get_user_name($this->session->email));
            $this->session->set_userdata('user_id', $this->client->get_user_id_from_username($this->session->username));
            $this->session->set_userdata('logged_in', '1');
            $this->session->set_userdata('isAdmin', $this->client->isAdmin($this->client->get_user_name($this->session->email)));
            return TRUE;
        }
        $this->form_validation->set_message('check_database', 'Incorrect email or password.');
        return FALSE;
    }

}
