<?php defined('BASEPATH') OR exit('No direct script access allowed');

class Search extends CI_Controller {

    /**
	 * Index Page for this controller.
	 *
	 * Maps to the following URL
	 * 		http://example.com/index.php/welcome
	 *	- or -
	 * 		http://example.com/index.php/welcome/index
	 *	- or -
	 * Since this controller is set as the default controller in
	 * config/routes.php, it's displayed at http://example.com/
	 *
	 * So any other public methods not prefixed with an underscore will
	 * map to /index.php/welcome/<method_name>
	 * @see https://codeigniter.com/user_guide/general/urls.html
	 */
    public function __construct() {
        parent::__construct();
        $this->load->helper(['form', 'url']);
        $this->load->library('session');
        $this->load->database();
    }
    public function index(){
        $this->load->model('server');
        $data['name'] = $this->input->post('name');
        $data['mangas'] = $this->server->get_like($data['name']);
        if(empty($data['mangas'])){
            $data['mangas'] = array();
        }
		$this->load->view('header');
        $this->load->view('search', $data);
        $this->load->view('footer');
    } 
    private function test_unit($mname){
        $this->load->model('server');
        if ($amount = $this->server->get_like($mname)) {
            return sizeof($amount);
        }else{
            return FALSE;
        }
    }
    public function test(){
        $this->load->model('server');
        $this->load->library('unit_test');
        
        $search_1 = $this->test_unit("");
        echo "Search Function Test (search '')";
        $allmangas = sizeof($this->server->get_mangas());
        echo $this->unit->run($search_1, $allmangas,"search empty return all manga (search count: " . $search_1 . " all: " . $allmangas . ")");
        
        $search_2 = $this->test_unit("a!@#!$!@#");
        echo "Search Function Test (search 'a!@#!$!@#')";
        echo $this->unit->run($search_1, 'is_int',"search must always return int");
        
    }
}