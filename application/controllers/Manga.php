<?php defined('BASEPATH') OR exit('No direct script access allowed');

class Manga extends CI_Controller {

    /**
	 * Index Page for this controller.
	 *
	 * Maps to the following URL
	 * 		http://example.com/index.php/welcome
	 *	- or -
	 * 		http://example.com/index.php/welcome/index
	 *	- or -
	 * Since this controller is set as the default controller in
	 * config/routes.php, it's displayed at http://example.com/
	 *
	 * So any other public methods not prefixed with an underscore will
	 * map to /index.php/welcome/<method_name>
	 * @see https://codeigniter.com/user_guide/general/urls.html
	 */
	 
    function _remap($params) {
        $this->index($params);
    }
    public function __construct() {
        parent::__construct();
        $this->load->helper(['form', 'url']);
        $this->load->library('session');
        $this->load->database();
    }
    private function test_unit($mname){
        $this->load->model('server');
        if ($this->server->isDup($mname)) {
            return TRUE;
        }else{
            return FALSE;
        }
    }
    public function index($params){

            $this->load->model('server');
            if($stars = $this->input->post('rating')){
                if($stars > 5){
                    $stars = 5;
                }
                if($stars < 0){
                    $stars = 0;
                }
                if($this->server->update_rating($this->session->user_id,$params,(int)$stars)){
                    unset($_POST['rating']);
                    redirect('manga/'.$params);    
                }
            }else{
                if($data['manga'] = $this->server->get_manga($params)){
                    $genres = explode(';',$data['manga']['genre']);
                    $data['genre'] = $this->server->get_genre_from_array($genres);
                    $data['chapters'] = $this->server->get_chapters($params);
                    $data['star'] = $this->server->get_star($this->session->user_id,$params);
                    $data['total'] = $this->server->get_total_star($params);
                    #die(var_dump($data['total']));
                    $this->load->view('header');
                    $this->load->view('mangainfo', $data);
                    $this->load->view('footer');
                }else{
                    redirect(base_url());
                    
                }
            }
        
    }   
}