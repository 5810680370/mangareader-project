<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Read extends CI_Controller {

	/**
	 * Index Page for this controller.
	 *
	 * Maps to the following URL
	 * 		http://example.com/index.php/welcome
	 *	- or -
	 * 		http://example.com/index.php/welcome/index
	 *	- or -
	 * Since this controller is set as the default controller in
	 * config/routes.php, it's displayed at http://example.com/
	 *
	 * So any other public methods not prefixed with an underscore will
	 * map to /index.php/welcome/<method_name>
	 * @see https://codeigniter.com/user_guide/general/urls.html
	 */
    public function __construct()
    {
        parent::__construct();
        $this->load->helper(['form', 'url']);
        $this->load->library('session');
    }
    function _remap($method, $params = array())
    {
        $this->index($method, $params);
    }
	public function index($manga, $chapter)
	{
	    $this->load->model('server');
        $data['mangas'] = $this->server->get_manga($manga);
	    $data['raw'] = $this->server->get_images($manga,$chapter[0]);
	    // SELECT * FROM `manga` JOIN `chapter` ON `manga`.`Id` = `chapter`.`Manga_Id` WHERE `chapter`.`Id` = Array AND `manga`.`Name` = 'test_manga1'
	    // https://mangareader-gram-gramkung.cs50.io/read/test_manga1/1/
	
	    $data['images'] = preg_split("[;]",$data['raw']['images']);
	    #die(var_dump($data['raw']));
		$this->load->view('header');
        $this->load->view('read', $data);
        $this->load->view('footer');
	}
}
