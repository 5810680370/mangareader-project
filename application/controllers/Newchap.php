<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Newchap extends CI_Controller {

	/**
	 * Index Page for this controller.
	 *
	 * Maps to the following URL
	 * 		http://example.com/index.php/welcome
	 *	- or -
	 * 		http://example.com/index.php/welcome/index
	 *	- or -
	 * Since this controller is set as the default controller in
	 * config/routes.php, it's displayed at http://example.com/
	 *
	 * So any other public methods not prefixed with an underscore will
	 * map to /index.php/welcome/<method_name>
	 * @see https://codeigniter.com/user_guide/general/urls.html
	 */
	public function __construct()
    {
        parent::__construct();
        $this->load->helper(['form', 'url']);
        $this->load->library('session');
        $this->load->database();
    }
	public function index()
	{
		if(!$this->session->logged_in){
            redirect(base_url());
        }
        $this->load->model('server');
        $data['mangas'] = $this->server->get_user_mangas($this->session->user_id);

		$this->load->view('header');
		$this->load->view('newchapter',$data);
		$this->load->view('footer');
	}

    public function test(){
        $this->load->library('unit_test');
        $this->load->model('server');

        $dup_name = $this->server->isDup('http://abc.com');
        echo "New Episode Function Test | Add manga name 'http://abc.com' (exists)";
        echo $this->unit->run($dup_name, TRUE,"Deny Duplicate Name");

        $dup_name2 = $this->server->isDup('http://def.com');
        echo "New Episode Function Test | Add manga name 'http://def.com' (not exists)";
        echo $this->unit->run($dup_name2, FALSE,"Allow valid Name");
    }
    public function insert() {
        $this->load->model('server');
        $this->load->library('form_validation');

        $data['post']=$this->input->post();
        $this->form_validation->set_rules('Name',
            'Name',
            'required');

        // $this->form_validation->set_rules('images',
        //     'images',
        //     'required');

        

        if ($this->form_validation->run()==FALSE) {
            redirect(base_url("/newchap"));
        }
        else {

            $query['Name']=$data['post']['Name'];
            $query['images'] = "";
            foreach($data['post']['images'] as $item)
            {
            $query['images'] .= $item.";";
            }
            $query['Manga_Id']=$data['post']['Manga_Id'];
            $query['userId'] = $this->session->user_id;
            if ($result = $this->server->insert_episode($query)) {
                redirect(base_url());
            }
            else {
                redirect(base_url("/newchap"));
            }
        }
    }
}
