<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Edit extends CI_Controller {

	/**
	 * Index Page for this controller.
	 *
	 * Maps to the following URL
	 * 		http://example.com/index.php/welcome
	 *	- or -
	 * 		http://example.com/index.php/welcome/index
	 *	- or -
	 * Since this controller is set as the default controller in
	 * config/routes.php, it's displayed at http://example.com/
	 *
	 * So any other public methods not prefixed with an underscore will
	 * map to /index.php/welcome/<method_name>
	 * @see https://codeigniter.com/user_guide/general/urls.html
	 */
    function _remap($method, $params = array())
    {
        if ($method == "updater") {
            $this->updater();
        } elseif ($method == "index") {
            redirect(base_url());
        } else {
            $this->index($method, $params);
        }
    }
	public function __construct()
    {
        parent::__construct();
        $this->load->helper(['form', 'url']);
        $this->load->library('session');
        $this->load->database();
    }
	public function index($method, $params)
	{
	    if(!$this->session->logged_in){
            redirect(base_url());
        }
        
        	$this->load->model('server');
        	$data['genres'] = $this->server->get_genre();
        $data['mangas'] = $this->server->get_manga($method);
        
        if(!$this->server->isOwner($this->session->user_id,$data['mangas']['Name'])) {
                redirect('home');
            }
            
        // die(var_dump($data['mangas']));
		$this->load->view('header');
		$this->load->view('editmanga',$data);
		$this->load->view('footer');
	}
	 public function updater() {
	    
        $this->load->model('server');
        $this->load->library('form_validation');


        $data['post']=$this->input->post();
   if(!$this->server->isOwner($this->session->user_id,$data['post']['Name'])) {
                redirect('home');
            }
        $this->form_validation->set_rules('Name',
        'Name',
        'required');
        $this->form_validation->set_rules('Display',
        'Display Name',
        'required');
        $this->form_validation->set_rules('Author',
        'Author Name',
        'required');
        $this->form_validation->set_rules('Description',
        'Description',
        'required');
       
        if(empty($data['post']['allgenres'])){
             redirect(base_url("/edit/$manganame"));
        }

        $cover="https://via.placeholder.com/150x200";
        $manganame = $this->server->get_manga_by_id($data['post']['Id']);
        $genre=implode(";",$data['post']['allgenres']);
        if ($this->form_validation->run()==FALSE) {
            redirect(base_url("/edit/$manganame"));
        }
        else {
            $query['Display']=$data['post']['Display'];
            $query['Name']=$data['post']['Name'];
            $query['Author']=$data['post']['Author'];
            $query['Status']=$data['post']['Status'];
            $query['Description']=$data['post']['Description'];
            $query['genre']=$genre;
            $query['coverurl']=$cover;
            $query['userId']=$this->session->user_id;
            if ($this->server->update_manga($query,$data['post']['Id'])) {
                redirect(base_url());
            }
            else {
               redirect(base_url("/edit/$manganame"));
            }
        }
    }
}
