<?php defined('BASEPATH') OR exit('No direct script access allowed');

class Delete extends CI_Controller {

    /**
	 * Index Page for this controller.
	 *
	 * Maps to the following URL
	 * 		http://example.com/index.php/welcome
	 *	- or -
	 * 		http://example.com/index.php/welcome/index
	 *	- or -
	 * Since this controller is set as the default controller in
	 * config/routes.php, it's displayed at http://example.com/
	 *
	 * So any other public methods not prefixed with an underscore will
	 * map to /index.php/welcome/<method_name>
	 * @see https://codeigniter.com/user_guide/general/urls.html
	 */
    function _remap($method, $params = array())
    {
        $this->index($method, $params);
    }
    public function __construct() {
        parent::__construct(); // Load form and url helper
        $this->load->helper(array('form', 'url'));
        $this->load->library('session');
        $this->load->database();
    }
    private function test_unit($uid,$mname){
        $this->load->model('server');
        if ($this->server->isOwner($uid, $mname)) {
            return TRUE;
        }else{
            return FALSE;
        }
    }
    public function index($manga, $chapter) {
        $this->load->model('server');
        if($manga == "test"){
            $this->load->library('unit_test');
            
            $delete_good1 = $this->test_unit("1","test_manga1");
            echo "Delete Function Test (userid: 1 | manga: test_manga1)";
            echo $this->unit->run($delete_good1, TRUE,"Allow owner to delete");
            
            $delete_good2 = $this->test_unit("1","test_manga2");
            echo "Delete Function Test (userid: 1 | manga: test_manga2)";
            echo $this->unit->run($delete_good2, TRUE,"Allow owner to delete");
            
            $delete_bad1 = $this->test_unit("11","test_manga2");
            echo "Delete Function Test (userid: 11 | manga: test_manga2)";
            echo $this->unit->run($delete_bad1, FALSE,"Deny member to delete");
            
            $delete_bad2 = $this->test_unit("21","test_manga2");
            echo "Delete Function Test (userid: 21 | manga: test_manga2)";
            echo $this->unit->run($delete_bad2, FALSE,"Deny member to delete");
            
        }else{
            
            if(!$this->server->isOwner($this->session->user_id,$manga)) {
                redirect('home');
            } // Load form validation
            
            // https://mangareader-gram-gramkung.cs50.io/delete/manga_1/         Delete Manga
            // https://mangareader-gram-gramkung.cs50.io/delete/manga_1/81       Delete Chapter
            if(empty($chapter)){
                $this->server->delete_with_manga($manga);
                if($this->server->delete_manga($manga)){
                    redirect('home');
                }
            }else{
               
                if($this->server->delete_chapter_manga($manga,$chapter[0])){
                    redirect('home');
                }
            }
            redirect('home');
        }

    }
}
