<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Server extends CI_Model
{
    public function __construct()
    {
        parent::__construct();
    }
    public function get_display($name)
    {
        $manga = $this->get_manga($name);
        return $manga['Display'];
    }
    

    public function get_genre_from_array($array){
        $data = array();
        foreach($array as $id){
            $this->db->where('Id', $id);
            $query = $this->db->get('genre');
            if ($query->num_rows() > 0) {
                $row = $query->row();
                $data[] = [
                    'Id' => $row->Id,
                    'Name' => $row->Name
                ];
                
            }
        }
        return $data;
    }
    public function update_rating($userId,$manga_name,$rate){
        $mangaid = $this->get_manga($manga_name)['Id'];
        $data = [
            'Manga_Id' => $mangaid,
            'User_Id' => $userId,
            'Rating' => $rate
        ];
        $this->db->where('User_Id', $userId);
        $this->db->where('Manga_Id', $mangaid);
        $query = $this->db->get('rating');
        if ($query->num_rows() > 0) {
            $data = [
                'Rating' => $rate
            ];
            $this->db->where('User_Id', $userId);
            $this->db->where('Manga_Id', $mangaid);
            return $this->db->update('rating', $data);
        }else{
            $data = [
                'Manga_Id' => $mangaid,
                'User_Id' => $userId,
                'Rating' => $rate
            ];
            return $this->db->insert('rating', $data);
        }
        return false;
    }
    public function get_total_star($name){
        $mangaid = $this->get_manga($name)['Id'];
        $this->db->where('Manga_Id', $mangaid);
        $query = $this->db->get('rating');
        $total = 0.0;
        $ppl = 0;
        foreach ($query->result() as $row) {
            $total += $row->Rating;
            $ppl += 1;
        }
        if (is_null($ppl) || $ppl<=0)
        {
            $avgvalue=0;
        }
        else{
            $avgvalue = $total/$ppl;
        }
        $data = ['total'=>$total,'ppl'=>$ppl,'avg'=>$avgvalue];
        return $data;
    }
    public function get_star($id,$name){
        $mangaid = $this->get_manga($name)['Id'];
        $this->db->where('Manga_Id', $mangaid);
        $this->db->where('User_Id', $id);
        $query = $this->db->get('rating');
        if ($query->num_rows() > 0) {
            $row = $query->row();
            return $row->Rating;
        }
        return 0;
    }
    public function isOwner($id,$manganame){
        $this->db->where('Name', $manganame);
        $this->db->where('userId', $id);
        $query = $this->db->get('manga');
        if ($query->num_rows() > 0) {
            return TRUE;
        }
        return FALSE;
    }
    public function delete_manga($manganame){
        $mangaid = $this->get_manga($manganame)['Id'];
        if($this->delete_rating($mangaid)){
            $this->db->where('Name', $manganame);
            return $this->db->delete('manga');
        }
    }
    public function delete_rating($mangaid){
       $this->db->where('Manga_Id', $mangaid);
       return $this->db->delete('rating');
    }
    public function get_images($manga_name,$chapter_id){

        $this->db->where('chapter.Id',$chapter_id);
        $this->db->where('manga.Name',$manga_name);
        $this->db->join('chapter', 'manga.Id = chapter.Manga_Id');
        $query = $this->db->get('manga');
        #print_r($this->db->last_query());
        #exit();
        if ($query->num_rows() > 0) {
            $row = $query->row();
            $data = [
                'images' => $row->images,
            ];
            return $data;
        }
        return FALSE;
    }
    public function get_manga($name)
    {
        $this->db->where('Name', $name);
        $query = $this->db->get('manga');
        if ($query->num_rows() > 0) {
            $row = $query->row();
            $data = [
                'Id' => $row->Id,
                'Name' => $row->Name,
                'Display' => $row->Display,
                'Author' => $row->Author,
                'Description' => $row->Description,
                'Status' => $row->Status,
                'userId' => $row->userId,
                'coverurl' => $row->coverurl,
                'genre' => $row->genre
            ];
            return $data;
        }
        return FALSE;
    }
    public function get_manga_by_id($id)
    {
        $this->db->where('Id', $id);
        $query = $this->db->get('manga');
        if ($query->num_rows() > 0) {
            $row = $query->row();
            $data = [
                'Id' => $row->Id,
                'Name' => $row->Name,
                'Display' => $row->Display,
                'Author' => $row->Author,
                'Description' => $row->Description,
                'Status' => $row->Status,
                'userId' => $row->userId,
                'coverurl' => $row->coverurl,
                'genre' => $row->genre
            ];
            return $data;
        }
        return FALSE;
    }
    public function get_chapters($name)
    {
        $manga = $this->get_manga($name);
        $this->db->order_by('Date', 'DESC');
        $this->db->where('Manga_Id', $manga['Id']);
        $query = $this->db->get('chapter');
        if ($query->num_rows() > 0) {
            foreach ($query->result() as $row) {
                $data[] = [
                    'Id' => $row->Id,
                    'Manga_Id' => $row->Manga_Id,
                    'Manga_Name' => $manga['Display'],
                    'Manga_NameId' => $manga['Name'],
                    'Name' => $row->Name,
                    'Date' => $row->Date,
                    'userId' => $row->userId
                ];
            }
            return $data;
        }
        return FALSE;
    }
    public function get_like($manga_name)
    {
        $this->db->like('Display', $manga_name);
        $query = $this->db->get('manga');
        if ($query->num_rows() > 0) {
            foreach ($query->result() as $row) {
                $data[] = [
                    'Id' => $row->Id,
                    'Name' => $row->Name,
                    'Display' => $row->Display,
                    'Author' => $row->Author,
                    'Description' => $row->Description,
                    'Status' => $row->Status,
                    'userId' => $row->userId,
                    'coverurl' => $row->coverurl,
                    'genre' => $row->genre
                ];
            }
            return $data;
        }
    }
    public function get_user_mangas($id){
        $this->db->where('userId', $id);
        $query = $this->db->get('manga');
        if($query->num_rows() > 0){
            foreach ($query->result() as $row) {
                $data[] = [
                    'Id' => $row->Id,
                    'Name' => $row->Name,
                    'Display' => $row->Display,
                    'Author' => $row->Author,
                    'Description' => $row->Description,
                    'Status' => $row->Status,
                    'userId' => $row->userId,
                    'coverurl' => $row->coverurl,
                    'genre' => $row->genre
                ];
            }
            return $data;
        }
        return FALSE;
    }
    public function get_mangas(){
        $query = $this->db->get('manga');
        if($query->num_rows() > 0){
            foreach ($query->result() as $row) {
                $data[] = [
                    'Id' => $row->Id,
                    'Name' => $row->Name,
                    'Display' => $row->Display,
                    'Author' => $row->Author,
                    'Description' => $row->Description,
                    'Status' => $row->Status,
                    'userId' => $row->userId,
                    'coverurl' => $row->coverurl,
                    'genre' => $row->genre
                ];
            }
            return $data;
        }
        return FALSE;
    }
    public function get_genre(){
        $query = $this->db->get('genre');
        if($query->num_rows() > 0){
            foreach ($query->result() as $row) {
                $data[] = [
                    'Id' => $row->Id,
                    'Name' => $row->Name
                ];
            }
            return $data;
        }
        return FALSE;
    }
    
    public function get_lastest()
    {
        $this->db->order_by('Date', 'DESC');
        //$this->db->order_by('manga.Id', 'DESC');
        // เอาแค่เวลาพอ อันนี้เอาอันล่าสุดK ครับ
        $this->db->join('manga', 'manga.Id = chapter.Manga_Id');
        $query = $this->db->get('chapter');
        if ($query->num_rows() > 0) {
            foreach ($query->result() as $row) {
        //   print_r($row);
                $manga_name_tmp = $row->images;
                $data[] = [
                    'Id' => $row->Id,
                    'Manga_Id' => $row->Manga_Id,
                    'Manga_Name' => $row->Name,
                    'Manga_Display' => $row->Display,
                    // 'Manga_Name' => $manga_name_tmp,
                    //'Manga_Display' => $this->get_display(str_split($manga_name_tmp,";")[0]),
//                    'Image' => $this->get_cover($row->Manga_Name),
                    'Name' => $row->Name,
                    'Date' => $row->Date,
                    'userId' => $row->userId
                ];
            }
            return $data;
        }
        return FALSE;
    }
    public function isDup($name){
        $this->db->where('Name', $name);
        $query = $this->db->get('manga');
        $total = $query->num_rows();
        return $total;
    
    }
  public function update_manga($data,$id)
    {
        // if($this->isDup($data['Name'])){
        //     return FALSE;
            
        // }
        // $this->db->where('Name', $data['Name']);
        // $this->db->or_where('Display', $data['Display']);
  
        $this->db->set('Display', $data['Display']);
       $this->db->set('Name', $data['Name']);
       $this->db->set('Author', $data['Author']);
       $this->db->set('Status', $data['Status']);
         $this->db->set('Description', $data['Description']);
       $this->db->set('genre', $data['genre']);
       $this->db->set('coverurl', $data['coverurl']);
       $this->db->set('userId', $data['userId']);
       $this->db->where('Id', $id); //which row want to upgra
       
        return $this->db->update('manga');
    }
    public function insert_manga($data)
    {
        if($this->isDup($data['Name'])){
            return FALSE;
            
        }
        $this->db->where('Name', $data['Name']);
        $this->db->or_where('Display', $data['Display']);
        $query = $this->db->get('manga');
        if ($query->num_rows() >= 1) {
            return FALSE;
        }
        return $this->db->insert('manga', $data);
    }
    public function insert_episode($data)
    {
        if(is_null($data['Name']) or $data['Name']=="")
        {
             return FALSE;
        }
       // print_r($data);
        return $this->db->insert('chapter', $data);
    }
    public function delete_with_manga($manganame)
    {
            $mangaid = $this->get_manga($manganame)['Id'];
            $this->db->where('Manga_Id', $mangaid);
            return $this->db->delete('chapter');
        
    }
    public function delete_chapter_manga($manganame,$chapter_id)
    {
            $mangaid = $this->get_manga($manganame)['Id'];
             
            $this->db->where('Manga_Id', $mangaid)->where('Id', $chapter_id);
        
     
       
            return $this->db->delete('chapter');
    }
}