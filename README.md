สมาชิก
ศิรกานต์ อันประนิตย์ 5810680370
ธนภัทร วงธ์แหวน 5910613297
บุญณัฐ พึ่งแย้มศรวล 5910613305
พรหมสุรินท์ พุทธรรมวงศ์ 5910613313

Pivotal Tracker: https://www.pivotaltracker.com/n/projects/2216020
Heroku Url: https://vast-dusk-35840.herokuapp.com/


การใช้งานตัว Unit Test
1. cd ~/workspace/application/tests/

--------------------------------------------------
Iteration 1
Login Test:
php phpunit.phar libraries/iteration1/TLogin.php

Register Test:
php phpunit.phar libraries/iteration1/TRegister.php

NewManga Test:
php phpunit.phar libraries/iteration1/TNewmanga.php
--------------------------------------------------
Iteration2
Search Test:
php phpunit.phar libraries/iteration2/TSearch.php
--------------------------------------------------
Iteration3
Addepisode Test:
php phpunit.phar libraries/iteration3/TAddepisode.php
--------------------------------------------------