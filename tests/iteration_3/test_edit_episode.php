<?php

class test_creat_episode extends PHPUnit\Framework\TestCase{
    
    
    /** @test */
    public function ep_is_number(){
        $user = new \mangareader_project\application\controller\Newchap;
        $this->assertFalse($user->set_Ep('A'));
        $this->assertFalse($user->set_Ep('@'));
        $this->assertFalse($user->set_Ep('_'));
        $this->assertFalse($user->set_Ep('!!!'));
        $this->assertFalse($user->set_Ep(' _123'));
        $this->assertFlase($user->set_Ep('3A'));
        $this->assertTrue($user->set_Ep('123'));
        $this->assertTrue($user->set_Ep('2'));
        
    }
    
    /** @test */
     public function name_ep_is_firstCharacter_only(){
        $user = new \mangareader_project\application\controller\Newchap;
        $this->assertFalse($user->edit_Name('12345Test'));
        $this->assertFalse($user->edit_Name('!Test'));
        $this->assertFalse($user->edit_Name('_Test'));
        $this->assertFalse($user->edit_Name(' Test'));
        $this->assertTrue($user->edit_Name('Test'));
    }
}